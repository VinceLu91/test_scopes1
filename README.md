== README

This is a follow up from https://bitbucket.org/VinceLu91/test_scopes/src

1. I managed to play with chained scopes. It turns out that you could just chain these scopes to search by various conditions
2. I somehow couldn't figure out how to create an object that belongs to another. This form of association will be for next exercise

Next steps:

1. ActiveRecord association: besides has many/belongs to relationships, how to make an object that belongs to another?