class AddCleanToShirt < ActiveRecord::Migration
  def change
    add_column :shirts, :clean, :boolean
  end
end
