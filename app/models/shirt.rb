class Shirt < ActiveRecord::Base
  belongs_to :person
  validates :color, presence: true
  validates :size, presence: {strict: true}
  validates_numericality_of :size, :greater_than_or_equal_to => 1
  scope :red, -> { where(color: 'red') }
  scope :blue, -> { where("size > ?", 3) and where(color: 'blue')}
  scope :dry_clean_only, -> { where(clean: true) }
end
