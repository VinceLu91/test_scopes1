json.array!(@shirts) do |shirt|
  json.extract! shirt, :id, :color, :size
  json.url shirt_url(shirt, format: :json)
end
